# VinylX

## About

VinylX is a simple Web Application, <br />
which continuously plays a piece of music and specifically the collection <br />
"Metallica-The Unforgiven".

VinylX was created with the following programming languages:
- HTML
- CSS
- Javascript

## License
VinylX is free to use with open source license.

## Project status

VinylX is available for testing online! <br />

## App

<p align="center"><img src="Vinyl88_Page_Preview.png" alt="Vinyl88_Page_Preview" width="300" height="250"/></p>
